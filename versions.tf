terraform {
  required_providers {
    bitwarden = {
      source  = "maxlaverse/bitwarden"
      version = ">= 0.1.1"
    }
    vultr = {
      source  = "vultr/vultr"
      version = ">= 2.15.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.18.1"
    }
    helm = {}
  }
}