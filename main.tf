locals {
  kube_config_path = fileexists(module.vultr_kubernetes_cluster.kube_config_path) ? module.vultr_kubernetes_cluster.kube_config_path : null

  namespaces = {
    "traefik" = {
      annotations = {
        "name.label.custom" = "some.value"
      }
      labels = {
        "example-label" = "this-is-a-value"
      }
    }
  }

  pvcs = {}

  dns_domains = {
    "preston_lol" = {
      domain = "preston.lol"
      ip     = null
    }
  }

  secrets = {
    "vultr_api_key"            = { id = "e67a4953-3763-4f3d-a6f9-afae005319ff" },
    "vultr_kubernetes_cluster" = { id = "44c6b241-37f2-4b76-aa90-b0150171ae2b" }
  }

  app_secrets = module.secrets.secrets

  vultr_kubernetes_cluster = {
    host                   = module.vultr_kubernetes_cluster.host
    cluster_ca_certificate = module.vultr_kubernetes_cluster.cluster_ca_certificate
    client_certificate     = module.vultr_kubernetes_cluster.client_certificate
    client_key             = module.vultr_kubernetes_cluster.client_key
  }
}

# grab secrets
module "secrets" {
  source  = "git::git@gitlab.com:666_iac/terraform-modules/custom/terraform-secrets.git"
  secrets = local.secrets
}

# Create the Cluster
module "vultr_kubernetes_cluster" {
  source = "git::git@gitlab.com:666_iac/terraform-modules/vultr/terraform-kubernetes_cluster.git"

  cluster_label = "test-cluster"
}

# Create default block storage for cluster
module "vultr_kubernetes_pvcs" {
  source   = "git::git@gitlab.com:666_iac/terraform-modules/kubernetes/terraform-pvc.git"
  for_each = local.pvcs

  name               = each.key
  capacity           = each.value.capacity
  storage_class_name = each.value.storage_class_name

  depends_on = [
    module.vultr_kubernetes_cluster
  ]
}

# Build the base/core namespaces
module "vultr_kubernetes_namespaces" {
  source   = "git::git@gitlab.com:666_iac/terraform-modules/kubernetes/terraform-namespace.git"
  for_each = local.namespaces

  name        = each.key
  annotations = each.value.annotations
  labels      = each.value.labels

  depends_on = [
    module.vultr_kubernetes_cluster
  ]
}

# Create DNS Domains/Zones
module "vultr_dns_domains" {
  source   = "git::git@gitlab.com:666_iac/terraform-modules/vultr/terraform-dns_domainb.git"
  for_each = local.dns_domains

  domain = each.value.domain
  ip     = each.value.ip
}



