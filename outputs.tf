output "vultr_kubernetes_pvcs" {
  value = module.vultr_kubernetes_pvcs
}

output "kube_config_path" {
  value = local.kube_config_path
}

output "vultr_dns_domains" {
  value = module.vultr_dns_domains
}