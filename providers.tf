provider "bitwarden" {
  master_password = var.bw_password
  client_id       = var.bw_client_id
  client_secret   = var.bw_client_secret
  email           = var.bw_email
  server          = var.bw_server
}

provider "kubernetes" {
  #config_path = local.kube_config_path
  host                   = local.vultr_kubernetes_cluster.host
  cluster_ca_certificate = base64decode(local.vultr_kubernetes_cluster.cluster_ca_certificate)
  client_certificate     = base64decode(local.vultr_kubernetes_cluster.client_certificate)
  client_key             = base64decode(local.vultr_kubernetes_cluster.client_key)
}

provider "vultr" {
  api_key     = local.app_secrets["vultr_api_key"].password
  rate_limit  = 100
  retry_limit = 3
}